package com.github.javpower.javavision.util.redis.vector;

public enum FieldType {
   TEXT,
   VECTOR,
   NUMBER
}