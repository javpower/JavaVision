package com.github.javpower.javavision.util.redis.vector;

public enum DistanceMetric {
    COSINE,
    L2,
    IP
}